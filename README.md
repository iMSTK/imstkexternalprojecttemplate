# iMSTKExternalProjectTemplate

This project template demonstrates usage of iMSTK externally. It provides two templates one can start with. One for iMSTK projects and another for a small superbuild.

It then also provides a look into how we use iMSTK, best practices, and is useful for consolidation across our projects.