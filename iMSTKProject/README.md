# iMSTKProject

This template example demonstrates how to link to iMSTK externally. One will first need to build iMSTK separately. Then this project.

## Build Instructions

 - Generate using cmake. Set the source directory to this one. Build directory to your prefered location.
 - Configure with iMSTK_DIR set to "<your imstk build directory>/install/lib/cmake/iMSTK-4.0". This directory contains the cmake configure file.
 - Generate the project
 - Copy dlls/libraries to from imstk install/bin to the build directory next to the exe. Alternatively add to the environment or system path. A superbuild instead would avoid this altogether.