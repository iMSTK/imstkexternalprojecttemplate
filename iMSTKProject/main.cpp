#include "imstkCamera.h"
#include "imstkKeyboardDeviceClient.h"
#include "imstkKeyboardSceneControl.h"
#include "imstkLight.h"
#include "imstkLogger.h"
#include "imstkMouseSceneControl.h"
#include "imstkNew.h"
#include "imstkRenderMaterial.h"
#include "imstkScene.h"
#include "imstkSceneManager.h"
#include "imstkSimulationManager.h"
#ifdef USE_RENDERING
#include "imstkVTKViewer.h"
#endif

using namespace imstk;

#define USE_HAPTICS

///
/// \brief This example is just for build testing
///
int
main(int argc, char** argv)
{
    // Write log to stdout and file
    Logger::startLogger();

    // Setup a scene
    imstkNew<Scene> scene("Scene");

    // Setup a scene manager to advance the scene
    imstkNew<SceneManager> sceneManager("Scene Manager");
    sceneManager->setActiveScene(scene);

    // Setup the SimulationManager to run all the modules
    imstkNew<SimulationManager> driver;
    driver->addModule(sceneManager);
    driver->setDesiredDt(0.015);

#ifdef USE_RENDERING
    // Setup a viewer to render
    imstkNew<VTKViewer> viewer("Viewer");
    viewer->setActiveScene(scene);
    driver->addModule(viewer);
#endif
    driver->start();

    return 0;
}