#include "imstkCamera.h"
#include "imstkHapticDeviceClient.h"
#include "imstkHapticDeviceManager.h"
#include "imstkIsometricMap.h"
#include "imstkKeyboardDeviceClient.h"
#include "imstkKeyboardSceneControl.h"
#include "imstkLight.h"
#include "imstkLineMesh.h"
#include "imstkLogger.h"
#include "imstkMeshIO.h"
#include "imstkMouseSceneControl.h"
#include "imstkNew.h"
#include "imstkPbdModel.h"
#include "imstkPbdObject.h"
#include "imstkRenderMaterial.h"
#include "imstkSceneManager.h"
#include "imstkSimulationManager.h"
#include "imstkSurfaceMesh.h"
#include "imstkVisualModel.h"
#include "imstkVTKRenderer.h"
#include "imstkVTKViewer.h"

#include "CustomScene.h"
#include "CustomTool.h"
#include "CustomToolControl.h"

using namespace imstk;

#define USE_HAPTICS

///
/// \brief This example demonstrates the cloth simulation
/// using Position based dynamics
///
int
main(int argc, char** argv)
{
    // Write log to stdout and file
    Logger::startLogger();

    // Setup a scene
    imstkNew<CustomScene> scene;

#ifdef USE_HAPTICS
    // Setup haptic device for control
    imstkNew<HapticDeviceManager>       hapticManager;
    hapticManager->setSleepDelay(1.0); // Delay for 1ms (haptics thread is limited to max 1000hz)
    std::shared_ptr<HapticDeviceClient> deviceClient = hapticManager->makeDeviceClient();
#else

#endif

    // Setup a scene manager to advance the scene
    imstkNew<SceneManager> sceneManager("Scene Manager");
    //sceneManager->setPrintTime(true);
    sceneManager->setActiveScene(scene);
    sceneManager->setExecutionType(Module::ExecutionType::ADAPTIVE);

    scene->setDevice(deviceClient);
    scene->loadScene();

    // Setup a viewer to render
    imstkNew<VTKViewer> viewer("Viewer");
    //viewer->setPrintTime(true);
    viewer->setActiveScene(scene);
    viewer->setVtkLoggerMode(VTKViewer::VTKLoggerMode::MUTE);
    std::dynamic_pointer_cast<VTKRenderer>(viewer->getActiveRenderer())->setAxesLength(0.03, 0.03, 0.03);

    // Setup the SimulationManager to run all the modules
    imstkNew<SimulationManager> driver;
    driver->addModule(viewer);
    driver->addModule(sceneManager);
#ifdef USE_HAPTICS
    driver->addModule(hapticManager);
#endif
    driver->setDesiredDt(0.015);

    // Add mouse and keyboard controls to the viewer
    {
#ifdef USE_HAPTICS
        imstkNew<VESSCustomHapticControl> hapticControl(hapticDeviceClient);
        hapticControl->setVESSScene(scene);
        scene->addController(hapticControl);
#else
        imstkNew<CustomMouseSceneControl> mouseControl(viewer->getMouseDevice());
        mouseControl->setSceneManager(sceneManager);
        viewer->addControl(mouseControl);
#endif

        // imstkNew<CustomKeyboardControl> keyControl(viewer->getKeyboardDevice());
        // keyControl->setModuleDriver(driver);
        // viewer->addControl(keyControl);
    }

    driver->start();

    return 0;
}
