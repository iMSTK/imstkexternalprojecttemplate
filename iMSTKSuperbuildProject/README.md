# iMSTKSuperbuildProject

This project provides a simple template for superbuilds we've been using in other projects. It contains an inner and outer build.

 - The outer build will build all external dependencies along with the inner. You may add all 3rd party dependencies here.

 - The inner is just your project which would link to all your dependencies.

With a superbuild users don't have to go find external dependencies.

For upstream development it is useful to have the option to instead use an external iMSTK repository. This can be done by setting USE_SYSTEM_iMSTK to true and setting iMSTK_DIR to the directory of the iMSTKConfig.cmake file. This would be in imstk build directory/install/lib/cmake/iMSTK-4.0.

## Build Instructions

Template Project Build Instructions